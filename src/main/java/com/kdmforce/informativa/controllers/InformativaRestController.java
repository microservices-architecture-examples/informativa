package com.kdmforce.informativa.controllers;

import com.kdmforce.informativa.services.InformativaService;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class InformativaRestController {

    @Autowired
    private InformativaService informativaService;

    @GetMapping("/hello")
    public String getMessage(@RequestParam(value = "name") String name) {
        String rsp = "Hi " + name + " : responded on - " + new Date();
        return rsp;
    }

    @GetMapping(path="/informativa", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JSONArray> informativa() {
        return new ResponseEntity<JSONArray>(informativaService.findElements(), HttpStatus.OK);
    }
}

package com.kdmforce.informativa.services;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;

@Service
public class InformativaService {

    @Value("${informativa.mongodb.ip:localhost}")
    private String ip_address;

    @Value("${informativa.mongodb.port:1}")
    private int port;

    @Value("${informativa.mongodb.collection:collection}")
    private String collectionName;

    @Value("${informativa.mongodb.dbname:dbname}")
    private String dbname;

    @Value("${informativa.mongodb.enable}")
    private boolean enable;

    private JSONArray readByDbNameAndCollectionName(){
        MongoClient mongo = null;
        JSONArray jsonArray = new JSONArray();
        try {
            mongo = new MongoClient( ip_address , port );

            DB db = mongo.getDB(dbname);
            DBCollection table = db.getCollection(collectionName);
            DBCursor cursor = table.find();

            while (cursor.hasNext()) {
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(cursor.next().toString());
                jsonArray.add(jsonObject);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }


    public JSONArray findElements() {
        if(enable){
            return readByDbNameAndCollectionName();
        } else {
            return new JSONArray();
        }
    }
}